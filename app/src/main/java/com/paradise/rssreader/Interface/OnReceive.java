package com.paradise.rssreader.Interface;

import com.paradise.rssreader.model.RssObject;

public interface OnReceive {
  void onMessageReceive(RssObject rssObject, int position, String url);
}
