package com.paradise.rssreader.Interface;

import android.view.View;

public interface ItemClickListener {
void onClick(View view, int position);
}
