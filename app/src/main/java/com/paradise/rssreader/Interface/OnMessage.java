package com.paradise.rssreader.Interface;

import android.support.v4.app.Fragment;
import com.paradise.rssreader.model.RssObject;

public interface OnMessage {
  void sendMessage(Fragment fragment,RssObject rssObject, int position, String url);
}
