package com.paradise.rssreader.Interface;
import com.paradise.rssreader.model.Feed;
import com.paradise.rssreader.model.Item;
import com.paradise.rssreader.model.RssObject;
import java.util.List;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.http.GET;
import retrofit2.http.Query;
import retrofit2.http.GET;
import retrofit2.http.Url;

public interface RssAdapter {
  @GET Call <RssObject>  getData(@Url String url);
}