package com.paradise.rssreader.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.paradise.rssreader.model.Item;
import java.io.Serializable;
import java.util.List;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;

//public class Enclosure
//{
//  public string link { get; set; }
//}

public class RssObject {

  @SerializedName("status")
  @Expose
  private String status;
  @SerializedName("feed")
  @Expose
  private Feed feed;
  @SerializedName("items")
  @Expose
  private List<Item> items = null;

  /**
   * No args constructor for use in serialization
   *
   */
  public RssObject() {
  }

  /**
   *
   * @param items
   * @param status
   * @param feed
   */
  public RssObject(String status, Feed feed, List<Item> items) {
    super();
    this.status = status;
    this.feed = feed;
    this.items = items;
  }

  public String getStatus() {
    return status;
  }

  public void setStatus(String status) {
    this.status = status;
  }

  public Feed getFeed() {
    return feed;
  }

  public void setFeed(Feed feed) {
    this.feed = feed;
  }

  public List<Item> getItems() {
    return items;
  }

  public void setItems(List<Item> items) {
    this.items = items;
  }

}