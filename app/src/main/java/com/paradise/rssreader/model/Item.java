package com.paradise.rssreader.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import java.io.Serializable;
import java.util.List;
import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;
public class Item {

  @SerializedName("title")
  @Expose
  private String title;
  @SerializedName("pubDate")
  @Expose
  private String pubDate;
  @SerializedName("link")
  @Expose
  private String link;
  @SerializedName("guid")
  @Expose
  private String guid;
  @SerializedName("author")
  @Expose
  private String author;
  @SerializedName("thumbnail")
  @Expose
  private String thumbnail;
  @SerializedName("description")
  @Expose
  private String description;
  @SerializedName("content")
  @Expose
  private String content;
  @SerializedName("categories")
  @Expose
  private List<String> categories = null;

  /**
   * No args constructor for use in serialization
   *
   */
  public Item() {
  }

  /**
   *
   * @param content
   * @param guid
   * @param author
   * @param pubDate
   * @param title
   * @param thumbnail
   * @param description
   * @param link
   * @param categories
   */
  public Item(String title, String pubDate, String link, String guid, String author, String thumbnail, String description, String content, List<String> categories) {
    super();
    this.title = title;
    this.pubDate = pubDate;
    this.link = link;
    this.guid = guid;
    this.author = author;
    this.thumbnail = thumbnail;
    this.description = description;
    this.content = content;
    this.categories = categories;
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public String getPubDate() {
    return pubDate;
  }

  public void setPubDate(String pubDate) {
    this.pubDate = pubDate;
  }

  public String getLink() {
    return link;
  }

  public void setLink(String link) {
    this.link = link;
  }

  public String getGuid() {
    return guid;
  }

  public void setGuid(String guid) {
    this.guid = guid;
  }

  public String getAuthor() {
    return author;
  }

  public void setAuthor(String author) {
    this.author = author;
  }

  public String getThumbnail() {
    return thumbnail;
  }

  public void setThumbnail(String thumbnail) {
    this.thumbnail = thumbnail;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public String getContent() {
    return content;
  }

  public void setContent(String content) {
    this.content = content;
  }

  public List<String> getCategories() {
    return categories;
  }

  public void setCategories(List<String> categories) {
    this.categories = categories;
  }

}