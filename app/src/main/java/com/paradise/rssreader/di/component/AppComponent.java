package com.paradise.rssreader.di.component;

import com.paradise.rssreader.di.module.AppModule;
import com.paradise.rssreader.di.scope.AppScope;
import com.paradise.rssreader.presentation.view.MainActivity;
import com.paradise.rssreader.presentation.view.RssItemDetail;
import com.paradise.rssreader.presentation.view.RssListFragment;
import com.paradise.rssreader.presentation.view.RssListItemFragment;
import dagger.Component;

@AppScope
@Component(modules = AppModule.class)
public interface AppComponent {

    void inject(MainActivity activity);
    void inject(RssListFragment rssListFragment);
    void inject(RssListItemFragment rssListItemFragment);
    void inject(RssItemDetail rssItemDetail);

}
