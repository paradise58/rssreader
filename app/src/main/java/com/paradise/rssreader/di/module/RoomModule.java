package com.paradise.rssreader.di.module;

import android.arch.persistence.room.Room;
import com.paradise.rssreader.common.App;
import com.paradise.rssreader.data.AppDatabase;
import com.paradise.rssreader.data.LocalBD;
import com.paradise.rssreader.data.dao.RssUrlDao;
import com.paradise.rssreader.di.scope.AppScope;
import dagger.Module;
import dagger.Provides;

@Module
public class RoomModule {

    private String db_name = "AppDatabase";

    @Provides
    @AppScope LocalBD localBD(RssUrlDao rssUrlDao) {

        return new LocalBD(rssUrlDao);
    }

    @Provides RssUrlDao rssItemDao(App app) {
        return Room.databaseBuilder(app, AppDatabase.class, db_name).build().rssItemDao();
    }

}
