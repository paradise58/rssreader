package com.paradise.rssreader.di.module;

import android.content.Context;
import com.paradise.rssreader.common.App;
import com.paradise.rssreader.di.scope.AppScope;
import dagger.Module;
import dagger.Provides;

@Module(includes = {
        RoomModule.class})

public class AppModule {

    private final App app;

    public AppModule(App app) {
        this.app = app;
    }

    @Provides
    Context provideContext() {
        return app;
    }

    @Provides
    App provideApp() {
        return app;
    }

}
