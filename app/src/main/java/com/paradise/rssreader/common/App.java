package com.paradise.rssreader.common;

import android.app.Application;
import android.content.ContentProvider;
import android.content.Context;
import android.os.Build;
import android.support.multidex.MultiDexApplication;
import android.util.JsonReader;
import com.facebook.stetho.Stetho;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.paradise.rssreader.Interface.RssAdapter;
import com.paradise.rssreader.di.component.AppComponent;
import com.paradise.rssreader.di.component.DaggerAppComponent;
import com.paradise.rssreader.di.module.AppModule;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.simplexml.SimpleXmlConverterFactory;

public class App extends MultiDexApplication {
  protected static App app;
  private AppComponent component = DaggerAppComponent.builder().appModule(new AppModule(this)).build();
  private static RssAdapter rssAdapter;
  private Retrofit retrofit;

  /**
   * Called when the application is starting, before any activity, service,
   * or receiver objects (excluding content providers) have been created.
   *
   * <p>Implementations should be as quick as possible (for example using
   * lazy initialization of state) since the time spent in this function
   * directly impacts the performance of starting the first activity,
   * service, or receiver in a process.</p>
   *
   * <p>If you override this method, be sure to call {@code super.onCreate()}.</p>
   *
   * <p class="note">Be aware that direct boot may also affect callback order on
   * Android {@link Build.VERSION_CODES#N} and later devices.
   * Until the user unlocks the device, only direct boot aware components are
   * allowed to run. You should consider that all direct boot unaware
   * components, including such {@link ContentProvider}, are
   * disabled until user unlock happens, especially when component callback
   * order matters.</p>
   */
  @Override public void onCreate() {
    super.onCreate();
    app = this;
    Stetho.initializeWithDefaults(this);


  }

  public RssAdapter getApi() {

    retrofit = new Retrofit.Builder()
        .baseUrl("https://android.jlelse.eu/") //Базовая часть адреc
        .addConverterFactory(GsonConverterFactory.create()) //Конвертер, необходимый для преобразования JSON'а в объекты
        .build();
    rssAdapter = retrofit.create(RssAdapter.class); //Создаем объект, при помощи которого будем выполнять запросы

    return rssAdapter;
  }
  public static App getInstance() {
    return app;
  }

 public static AppComponent getAppComponent(Context context){
    return ((App)context.getApplicationContext()).component;
 }
}
