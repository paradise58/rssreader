package com.paradise.rssreader.presentation.view;

import android.arch.lifecycle.MutableLiveData;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.paradise.rssreader.presentation.adapter.RssListAdapter;
import com.paradise.rssreader.Interface.ItemClickListener;
import com.paradise.rssreader.R;
import com.paradise.rssreader.common.App;
import com.paradise.rssreader.data.LocalBD;
import com.paradise.rssreader.data.entity.RssUrl;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;
import java.util.List;
import javax.inject.Inject;

/**
 * A simple {@link Fragment} subclass.
 */
public class RssListFragment extends Fragment implements ItemClickListener {
  RecyclerView recyclerView;
  @Inject LocalBD localBD;
  MutableLiveData<List<RssUrl>> rssUrlList = new MutableLiveData<>();
  RecyclerView.Adapter adapter;

  public RssListFragment() {
    // Required empty public constructor
  }

  @Override public void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    App.getAppComponent(getContext()).inject(this);
  }


  @Override public View onCreateView(LayoutInflater inflater, ViewGroup container,
      Bundle savedInstanceState) {
    // Inflate the layout for this fragment
    View view = inflater.inflate(R.layout.fragment_rss_list, container, false);
    initRecyclerView(view);
    return view;
  }

  @Override public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
    super.onViewCreated(view, savedInstanceState);
    convertRxToLiveData();
    rssUrlList.observe(this, ((RssListAdapter) adapter)::setRssItemsList);
  }

  private void initRecyclerView(View view) {
    recyclerView = view.findViewById(R.id.recycler);
    adapter = new RssListAdapter(this);
    recyclerView.setAdapter(adapter);
    recyclerView.setLayoutManager(
        new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
    recyclerView.addItemDecoration(
        new DividerItemDecoration(getContext(), DividerItemDecoration.VERTICAL));
    recyclerView.setNestedScrollingEnabled(false);
    recyclerView.setHasFixedSize(false);
  }

  private void convertRxToLiveData() {
    new CompositeDisposable().add(localBD.getRssUrlDao()
        .getAll()
        .subscribeOn(Schedulers.io())
        .subscribe(result -> rssUrlList.postValue(result),
            error -> Log.e("loge", "convertRxToLiveData: ")));
  }

  @Override public void onClick(View view, int position) {
    Fragment rssListFragment = new RssListItemFragment();
    Bundle bundle = new Bundle();
    bundle.putInt("position", position);
    rssListFragment.setArguments(bundle);
    getFragmentManager().beginTransaction()
        .replace(R.id.fragment_container, rssListFragment)
        .addToBackStack("")
        .commit();
  }
}
