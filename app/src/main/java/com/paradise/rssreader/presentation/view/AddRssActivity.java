package com.paradise.rssreader.presentation.view;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;
import com.paradise.rssreader.R;

public class AddRssActivity extends AppCompatActivity {
  EditText title, link;
  public static final String RSS_LINK = "addRssLink";
  public static final String RSS_TITLE = "addRssTitle";


  @Override protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_add_rss);
    title = findViewById(R.id.rss_title);
    link = findViewById(R.id.rss_link);
  }

  public void onClick(View view) {
    if (!isfieldsEmpty()) {
      setResult(MainActivity.ADD_RSS, new Intent().putExtra(RSS_TITLE, title.getText().toString())
          .putExtra(RSS_LINK, link.getText().toString()));
      finish();
    } else {
      Toast.makeText(this, "Необходимо заполнить все поля", Toast.LENGTH_LONG).show();
    }
  }

  boolean isfieldsEmpty(){
    return title.getText().toString().equals("") && link.getText().toString().equals("");
  }
}
