package com.paradise.rssreader.presentation.view;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import android.widget.Toast;
import com.paradise.rssreader.model.Feed;
import com.paradise.rssreader.presentation.adapter.RssListItemAdapter;
import com.paradise.rssreader.Interface.ItemClickListener;
import com.paradise.rssreader.R;
import com.paradise.rssreader.common.App;
import com.paradise.rssreader.common.HTTPDataHandler;
import com.paradise.rssreader.data.LocalBD;
import com.paradise.rssreader.model.RssObject;
import javax.inject.Inject;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class RssListItemFragment extends Fragment implements ItemClickListener {
  RecyclerView recyclerView;
  RssObject rssObject;
  @Inject LocalBD localBD;
  int position;
  String url;
RssListItemAdapter adapter;
  public RssListItemFragment() {
    // Required empty public constructor
  }


  @Override public void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    App.getAppComponent(getContext()).inject(this);

  }

  @Override public View onCreateView(LayoutInflater inflater, ViewGroup container,
      Bundle savedInstanceState) {
    //Inflate the layout for this fragment
    View view = inflater.inflate(R.layout.fragment_rss_list_item, container, false);
    initRecyclerView(view);
    if (getArguments() != null) {
      position = getArguments().getInt("position");
    }

    new Thread(new Runnable() {
      @Override public void run() {
        url = localBD.getRssUrlDao().getRssUrlById(position + 1).getUrl();
        loadRss();
      }
    }).start();

    return view;
  }

  private void initRecyclerView(View view) {
    recyclerView = view.findViewById(R.id.recycler);
    recyclerView.setLayoutManager(
        new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
    recyclerView.addItemDecoration(
        new DividerItemDecoration(getContext(), DividerItemDecoration.VERTICAL));
    recyclerView.setNestedScrollingEnabled(false);
    recyclerView.setHasFixedSize(false);
    adapter = new RssListItemAdapter();
    recyclerView.setAdapter(adapter);

  }


  private void loadRss() {
    App.getInstance().getApi().getData(url).enqueue(new Callback<RssObject>() {
      @Override public void onResponse(Call<RssObject> call, Response<RssObject> response) {
        adapter.setRssObject(response.body(), RssListItemFragment.this);
        adapter.notifyDataSetChanged();
      }

      @Override public void onFailure(Call<RssObject> call, Throwable t) {
        System.out.println(t.getMessage());
      }
    });
  }


  @Override public void onClick(View view, int position) {
    Fragment rssItemDetail = new RssItemDetail();
    ((MainActivity)getActivity()).sendMessage(rssItemDetail,rssObject,position,url);
        getFragmentManager()
        .beginTransaction()
        .replace(R.id.fragment_container, rssItemDetail)
        .addToBackStack("1")
        .commit();
  }
}
