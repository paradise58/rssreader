package com.paradise.rssreader.presentation.view;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import android.widget.TextView;
import com.google.gson.Gson;
import com.paradise.rssreader.Interface.OnReceive;
import com.paradise.rssreader.R;
import com.paradise.rssreader.common.App;
import com.paradise.rssreader.common.HTTPDataHandler;
import com.paradise.rssreader.data.LocalBD;
import com.paradise.rssreader.model.RssObject;
import javax.inject.Inject;

/**
 * A simple {@link Fragment} subclass.
 */
public class RssItemDetail extends Fragment implements OnReceive {
  @Inject LocalBD localBD;
  RssObject rssObject;
  int position;
  TextView title, content, date;
  String url;

  public RssItemDetail() {
    // Required empty public constructor
  }

  @Override public void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    App.getAppComponent(getContext()).inject(this);
  }

  @Override public void onSaveInstanceState(@NonNull Bundle outState) {
    super.onSaveInstanceState(outState);
    outState.putInt("key", position);
  }

  @Override public View onCreateView(LayoutInflater inflater, ViewGroup container,
      Bundle savedInstanceState) {
    if (savedInstanceState != null) {
      position = savedInstanceState.getInt("key");
    }
    View view = inflater.inflate(R.layout.fragment_rss_item_detail, container, false);
    title = view.findViewById(R.id.title);
    content = view.findViewById(R.id.content);
    date = view.findViewById(R.id.date);
    new RssLoader().execute();
    return view;
  }

  @Override public void onMessageReceive(RssObject rssObject, int position, String url) {
    this.rssObject = rssObject;
    this.position = position;
    this.url = url;
  }

  class RssLoader extends AsyncTask<String, String, String> {
    ProgressDialog progressDialog = new ProgressDialog(requireContext());

    @Override protected void onPreExecute() {
      progressDialog.setMessage("Please wait ....");
      progressDialog.show();
    }

    @Override protected String doInBackground(String... strings) {
      return new HTTPDataHandler().getHTTPData(url);
    }

    @Override protected void onPostExecute(String s) {
      progressDialog.dismiss();
      rssObject = new Gson().fromJson(s, RssObject.class);
      title.setText(rssObject.getItems().get(position).getTitle());
      content.setText(format(rssObject.getItems().get(position).getDescription()));
      date.setText(rssObject.getItems().get(position).getLink());
    }

    private String format(String content) {
      return content.replace("<br>", "\n");
    }
  }
}
