package com.paradise.rssreader.presentation.view;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;
import com.paradise.rssreader.Interface.OnMessage;
import com.paradise.rssreader.Interface.OnReceive;
import com.paradise.rssreader.R;
import com.paradise.rssreader.common.App;
import com.paradise.rssreader.data.LocalBD;
import com.paradise.rssreader.model.RssObject;
import javax.inject.Inject;

public class MainActivity extends AppCompatActivity implements OnMessage {
  android.support.v7.widget.Toolbar toolbar;
  @Inject LocalBD localBD;
  SharedPreferences sharedPreferences;
  Boolean isFirstLaunch;
  FragmentManager fragmentManager;
  public static final int ADD_RSS = 0;

  @Override protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    App.getAppComponent(this).inject(this);
    setContentView(R.layout.activity_main);
    fragmentManager = getSupportFragmentManager();
    sharedPreferences = getSharedPreferences("firstLaunch", Context.MODE_PRIVATE);
    initToolBar();
    initFirstLaunch();
    if (isInternetConnected()) {
      goToRssListFragment();
    } else {
      showAlertDialog();
    }
  }

  private void showAlertDialog() {
    new AlertDialog.Builder(this).setTitle("Нет интернета, попробуй позже")
        .setPositiveButton("Ok", (dialog, view) -> MainActivity.this.finish()).create().show();
  }

  private boolean isInternetConnected() {
    ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
    return cm.getActiveNetworkInfo()!=null;
  }

  private void initFirstLaunch() {
    isFirstLaunch = sharedPreferences.getBoolean("isFirstLaunch", false);
    if (!isFirstLaunch) {
      localBD.insertDefaultRssToLocalBD();
      sharedPreferences.edit().putBoolean("isFirstLaunch", true).apply();
    }
  }


  private void goToRssListFragment() {
    if (fragmentManager.getBackStackEntryCount() > 0) {
      fragmentManager.beginTransaction()
          .replace(R.id.fragment_container, fragmentManager.getFragments().get(0))
          .commit();
    } else {
      fragmentManager.beginTransaction()
          .replace(R.id.fragment_container, new RssListFragment())
          .commit();
    }
  }

  private void initToolBar() {
    toolbar = findViewById(R.id.toolbar);
    toolbar.setTitle("News");
    setSupportActionBar(toolbar);
  }


  @Override public boolean onOptionsItemSelected(MenuItem item) {
    switch (item.getItemId()){
      case R.id.menu_add_rss:
        addRss();
        return true;
    }
    return super.onOptionsItemSelected(item);
  }

  private void addRss() {
    startActivityForResult(new Intent(MainActivity.this,AddRssActivity.class),ADD_RSS);
  }

  @Override
  protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
    super.onActivityResult(requestCode, resultCode, data);
    if(data != null) {
      localBD.insertRssUrl(data.getStringExtra(AddRssActivity.RSS_TITLE), data.getStringExtra(AddRssActivity.RSS_LINK));
      fragmentManager.beginTransaction().replace(R.id.fragment_container, new RssListFragment()).commit();
    }
    }

  @Override public boolean onCreateOptionsMenu(Menu menu) {
    getMenuInflater().inflate(R.menu.main_menu, menu);
    return true;
  }

  @Override
  public void sendMessage(Fragment fragment, RssObject rssObject, int position, String url) {
    ((OnReceive) fragment).onMessageReceive(rssObject, position, url);
  }

}
