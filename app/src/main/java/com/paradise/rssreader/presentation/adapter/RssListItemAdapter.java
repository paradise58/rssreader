package com.paradise.rssreader.presentation.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.paradise.rssreader.Interface.ItemClickListener;
import com.paradise.rssreader.R;
import com.paradise.rssreader.model.Item;
import com.paradise.rssreader.model.RssObject;
import java.util.List;

public class RssListItemAdapter extends RecyclerView.Adapter<RssListItemAdapter.RssItemViewHolder> {
  private ItemClickListener itemClickListener;
  private RssObject rssObject ;

  public RssListItemAdapter( ) {
  }

  public void setRssObject(RssObject rssObject,ItemClickListener itemClickListener) {
    this.rssObject = rssObject;
    this.itemClickListener = itemClickListener;
  }

  @NonNull @Override
  public RssListItemAdapter.RssItemViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup,
      int i) {
    View view =
        LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.list, viewGroup, false);
    return new RssItemViewHolder(view);
  }

  @Override public void onBindViewHolder(@NonNull RssItemViewHolder rssItemViewHolder, int i) {
    rssItemViewHolder.textView.setText(rssObject.getItems().get(i).getTitle());
  }

  @Override public int getItemCount() {
    if(rssObject == null) return 0;
    return rssObject.getItems().size();
  }

  class RssItemViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
    TextView textView;

    public RssItemViewHolder(@NonNull View itemView) {
      super(itemView);
      textView = itemView.findViewById(R.id.rss_list_item);
      itemView.setOnClickListener(this);
    }

    /**
     * Called when a view has been clicked.
     *
     * @param v The view that was clicked.
     */
    @Override public void onClick(View v) {
      itemClickListener.onClick(v,getAdapterPosition());
    }
  }
}
