package com.paradise.rssreader.presentation.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.paradise.rssreader.Interface.ItemClickListener;
import com.paradise.rssreader.R;
import com.paradise.rssreader.data.entity.RssUrl;
import java.util.ArrayList;
import java.util.List;

public class RssListAdapter extends RecyclerView.Adapter<RssListAdapter.RssListViewHolder> {
  private List<RssUrl> rssItemsList = new ArrayList<>();
  private ItemClickListener itemClickListener;

  public RssListAdapter(ItemClickListener itemClickListener) {
    this.itemClickListener = itemClickListener;
  }

  public void setRssItemsList(List<RssUrl> rssItemsList) {
    this.rssItemsList = rssItemsList;
    notifyDataSetChanged();
  }

  @NonNull @Override
  public RssListViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
    View view =
        LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.list, viewGroup, false);
    return new RssListViewHolder(view);
  }

  @Override public void onBindViewHolder(@NonNull RssListViewHolder rssListViewHolder, int i) {
    rssListViewHolder.rssListItem.setText(rssItemsList.get(i).getTitle());
  }

  @Override public int getItemCount() {
    return rssItemsList.size();
  }

  class RssListViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
    TextView rssListItem;

    public RssListViewHolder(@NonNull View itemView) {
      super(itemView);
      rssListItem = itemView.findViewById(R.id.rss_list_item);
      itemView.setOnClickListener(this);
    }

    /**
     * Called when a view has been clicked.
     *
     * @param v The view that was clicked.
     */
    @Override public void onClick(View v) {
      itemClickListener.onClick(v, getAdapterPosition());
    }
  }
}
