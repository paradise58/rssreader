package com.paradise.rssreader.data.dao;

import android.arch.lifecycle.MutableLiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import com.paradise.rssreader.data.entity.RssUrl;
import io.reactivex.Flowable;
import java.util.List;

@Dao public interface RssUrlDao {
  @Query("SELECT * FROM RssUrl") Flowable<List<RssUrl>> getAll();

  @Query("SELECT * FROM RssUrl WHERE id IN (:position)") RssUrl getRssUrlById(
      int position);

  @Insert void insert(RssUrl rssUrl);

}
