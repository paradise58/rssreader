package com.paradise.rssreader.data;

import com.paradise.rssreader.data.dao.RssUrlDao;
import com.paradise.rssreader.data.entity.RssUrl;
import javax.inject.Inject;

public class LocalBD {
  private final String RSS_link1 = "https://bash.im/rss/";
  private final String RSS_link2 = "http://novkamen.ru/news.rss";
  private final String RSS_link3 = "http://rss.nytimes.com/services/xml/rss/nyt/Science.xml";
  private final String Rss_to_Json_API = "https://api.rss2json.com/v1/api.json?rss_url=";
  private final RssUrlDao rssUrlDao;

  @Inject
  public LocalBD(RssUrlDao rssUrlDao) {
    this.rssUrlDao = rssUrlDao;
  }

  public void insertRssUrl( final String title, String url) {
  new Thread(()->rssUrlDao.insert(new RssUrl(title,Rss_to_Json_API+url))).start();
  }

  public RssUrlDao getRssUrlDao() {
    return rssUrlDao;
  }

  public void insertDefaultRssToLocalBD(){
    insertRssUrl("Bash org", RSS_link1);
    insertRssUrl("Каменская новь", RSS_link2);
    insertRssUrl("NewYork Times", RSS_link3);
  }

}
