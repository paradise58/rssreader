package com.paradise.rssreader.data;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;
import com.paradise.rssreader.data.dao.RssUrlDao;
import com.paradise.rssreader.data.entity.RssUrl;

@Database(entities = {RssUrl.class}, version = 1, exportSchema = false)
public abstract class DataBase extends RoomDatabase {
  public abstract RssUrlDao rssUrlDao();
}
